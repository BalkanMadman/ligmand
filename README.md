# ligmand
a C program which reads libvirt domain xml file and binds and unbinds pcie/usb devices of a running machine, making them usable during the rest of a lifecycle of a host  
might do more in the future

#### copyright and licencing
Copyright (c) 2023 Zurab Kvachadze

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

You indeed have. See ./COPYING
